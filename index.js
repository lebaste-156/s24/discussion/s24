// Node.js -> will provide us with a runtime environment (RTE) that will allow us to execute our program,

// RTE is the environment/system in which a program can be executed.

// TASK: Lets create a standard server setup using plain NodeJS

// Identify and prepare the components/ingredients needed in order to execute the task.
	// http (hypertext transfer protocol)
	// http - built-in module of NodeJs that will allow us to establish a connection and allow is to transfer data over the HTTP.

	// use a function called require() in order to desired module. repackage the module and store it inside a new variable.
let http = require("http");

	// http will provide us with all the components needed to establish a server.

	// createServer() - will allow us to create an HTTP server.

	// provide/instruct the server what to do.

	// Identify and describe a location where the connection will happen in order to provide a proper medium for both parties and bind the connection to the desired port number.
let port = 3000;

	// listen() - bind and listen to a specific whenver its being accessed by your computer.

	// Identify a point where we want to terminate the transmission using the end() function

http.createServer(function(request, response) {
	response.write('This is me responding back');
	response.end() //terminate the transmission	
}).listen(port);

	// create a response in the terminal to confirm if a connection has been successfully established.
	// 1. we can give a confirmation to the client that a connection is working
	// 2. we can specift what address/location was established.

console.log(`Server is running successfully on port: ${port}`);

